﻿using Chess.Calculations.Gameplay;
using Chess.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Chess.Forms
{
    /// <summary>
    /// Interaction logic for PawnChange.xaml
    /// </summary>
    public partial class PawnChange : Window
    {
        public PawnChange()
        {
            InitializeComponent();
            
                img_bishop.Source = new BitmapImage
                {
                    UriSource = new Uri("Resources/bishop_" + 
                    GameEvents.CurrentTeam.ToString().ToLower() + ".png", UriKind.Relative)
                };

                img_knight.Source = new BitmapImage
                {
                    UriSource = new Uri("Resources/knight_" +
                    GameEvents.CurrentTeam.ToString().ToLower() + ".png", UriKind.Relative)
                };

                img_rook.Source = new BitmapImage
                {
                    UriSource = new Uri("Resources/rook_" +
                    GameEvents.CurrentTeam.ToString().ToLower() + ".png", UriKind.Relative)
                };
                img_queen.Source = new BitmapImage
                {
                    UriSource = new Uri("Resources/queen_" +
                    GameEvents.CurrentTeam.ToString().ToLower() + ".png", UriKind.Relative)
                };    
        }

        private void btn_continue_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (chk_rook.IsChecked == true)
                Chessboard.chess.PawnTransform = "rook";
            else if (chk_knight.IsChecked == true)
                Chessboard.chess.PawnTransform = "knight";
            else if (chk_bishop.IsChecked == true)
                Chessboard.chess.PawnTransform = "bishop";
            else if (chk_queen.IsChecked == true)
                Chessboard.chess.PawnTransform = "queen";
            else
                e.Cancel = true;
        }
    }
}
