﻿using System.Collections.Generic;

namespace Chess.Object
{
    public abstract class Chessman
    {
        protected static int _nextId;
        protected int _id;
        protected Pieces _name;
        protected byte _location;
        protected bool _captured;
        protected Teams _team;
        protected Dictionary<int, byte> _history;
        protected sbyte[] _allowedMove;
        protected bool _captureInMove;
        protected System.Drawing.Bitmap _icon;
        protected bool _oneMove;
        protected bool _jumpOver = false;
        protected bool _fromPromotion = false;

        public int Id { get { return _id; } }
        public Pieces Name { get { return _name; } set { _name = value; } }
        public byte Location { get { return _location; } set { _location = value; } }
        public bool Captured { get { return _captured; } set { _captured = value; } }
        public Teams Team { get { return _team; } set { _team = value; } }
        public Dictionary<int, byte> HistoryLocations { get { return _history; } }
        public sbyte[] AllowedMove { get { return _allowedMove; } set { _allowedMove = value; } }
        public bool CaptureInMove { get { return _captureInMove; } set { _captureInMove = value; } }
        public bool OneMove { get { return _oneMove; } set { _oneMove = value; } }
        public bool JumpOver { get { return _jumpOver; } set { _jumpOver = value; } }
        public bool FromPromotion { get { return _fromPromotion; } set { _fromPromotion = value; } }
        public System.Drawing.Bitmap Icon { get { return _icon; } }

        public void AddPreviousLocation(int move, byte location)
        {
            _history.Add(move, location);
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public abstract bool CanMove(sbyte move);
        public abstract void AfterMove();
    }
}
