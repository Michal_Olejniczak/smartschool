﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Object
{
    public class Rook : Chessman 
    {
        public Rook(Teams team, byte Location)
        {
            _id = System.Threading.Interlocked.Increment(ref _nextId);
            _team = team;
            _captured = false;
            _captureInMove = true;
            _location = Location;
            _icon = (System.Drawing.Bitmap)Properties.Resources.ResourceManager.GetObject(
                string.Format(GetType().Name + "_" + team.ToString()).ToLower());
            //_icon = "Rook";
            _oneMove = false;
            _history = new Dictionary<int, byte>();
            _allowedMove = new sbyte[] {-8, -1, 1, 8};
        }

        public override string ToString()
        {
            return this.GetType().Name + "_" + _team.ToString();
        }

        public override bool CanMove(sbyte move)
        {
            if (!_allowedMove.Contains(move))
                return false;
            return true;
        }

        public override void AfterMove()
        {
            
        }
    }
}
