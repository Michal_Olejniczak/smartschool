﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Object
{
    public class Pawn : Chessman
    {
        public Pawn(Teams team, byte Location)
        {
            _id = System.Threading.Interlocked.Increment(ref _nextId);
            _team = team;
            _captured = false;
            _captureInMove = false;
            _location = Location;
            _icon = (System.Drawing.Bitmap)Properties.Resources.ResourceManager.GetObject(
                string.Format(GetType().Name + "_" + team.ToString()).ToLower());
            //_icon = "Pawn";
            _oneMove = true;
            _history = new Dictionary<int, byte>();
            if (team == Teams.White)
                _allowedMove = new sbyte[] { -1, -2};
            else
                _allowedMove = new sbyte[] { 1, 2};
        }

        public override bool CanMove(sbyte move)
        {
            if(!_allowedMove.Contains(move))
                return false;
            if (_team == Teams.White)
                _allowedMove = new sbyte[] { -1 };
            else
                _allowedMove = new sbyte[] { 1 };

            return true;
        }

        public override string ToString()
        {
            return this.GetType().Name + "_" + _team.ToString();
        }

        public override void AfterMove()
        {
            if (_history.Count > 1)
            {
                if (_team == Teams.White)
                    _allowedMove = new sbyte[] { -1 };
                else
                    _allowedMove = new sbyte[] { 1 };
            }
            else
            {
                if (_team == Teams.White)
                    _allowedMove = new sbyte[] { -1, -2 };
                else
                    _allowedMove = new sbyte[] { 1, 2 };
            }
        }
    }
}
