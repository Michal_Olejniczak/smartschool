﻿namespace Chess.Object
{
    public enum ChessmansValues
    {
        Pawn,
        Knight,
        Bishop,
        Rook,
        Queen,
        King
    }
}