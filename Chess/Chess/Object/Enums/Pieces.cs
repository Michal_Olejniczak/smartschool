﻿namespace Chess.Object
{
    public enum Pieces
    {
        Pawn,
        Knight,
        Bishop,
        Rook,
        Queen,
        King
    }
}
