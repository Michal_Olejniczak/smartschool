﻿namespace Chess.Object
{
    public enum MovesFinish
    {
        Move,
        Capture,
        Castling,
        EnPassant,
        Check = 101,
        Protection = 102
    }
}