﻿namespace Chess.Object
{
    public enum Moves
    {
        Horizontal,
        Vertical,
        DiagonalUp,
        DiagonalDown
    }
}