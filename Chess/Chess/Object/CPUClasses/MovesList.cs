﻿using System.Collections;
using System.Linq;

namespace Chess.Object
{ 
    public class MovesList: CollectionBase
    {
        public void Add(MoveMemory el)
        {
            List.Add(el);
        }

        public void Remove(int Index)
        {
            if (Index > Count - 1 || Index < 0)
            { }
            else
            { List.RemoveAt(Index); }
        }

        public MoveMemory Item(int Index)
        {
            var output = from MoveMemory in List.OfType<MoveMemory>()
                         where MoveMemory.Id == Index
                         select MoveMemory;
            return output.First();
        }
    }
}
