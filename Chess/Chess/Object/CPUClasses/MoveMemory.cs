﻿namespace Chess.Object
{
    public class MoveMemory
    {
        static int _nextId;
        private int _id;
        private Chessman _chessman;
        private byte _location;
        private MovesFinish? _moveResult;

        public int Id { get { return _id; } }
        public Chessman GetChessman  { get { return _chessman; } set { _chessman = value; } }
        public byte Location { get { return _location; } set { _location = value; } }
        public MovesFinish? Move { get { return _moveResult; } set { _moveResult = value; } }

        public MoveMemory(Chessman chessman, byte location, MovesFinish? move)
        {
            _id = System.Threading.Interlocked.Increment(ref _nextId);
            _chessman = chessman;
            _location = location;
            _moveResult = move;
        }
    }
}
