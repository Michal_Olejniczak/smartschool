﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Chess.Calculations.Mappers
{
    public static class BtnMapper
    {
        public static Button GetBtnFromloaction(byte location)
        {
            int row =  location % 8 + 1;
            int col = (int)Math.Floor((double)(location / 8)) + 1;

            return (Button)((MainWindow)Application.Current.MainWindow).
                gridBoard.Children
                .Cast<UIElement>()
                .FirstOrDefault(i => Grid.GetRow(i) == row &&
                Grid.GetColumn(i) == col);
        }

        public static byte GetLocationFromBtn(Button btn)
        {
            string btnName = btn.Name.ToString();
            btnName = btnName.Replace("btn_F", "");
            return (byte)(  
                (int.Parse(btnName[1].ToString())*8) - int.Parse(btnName[0].ToString()));
        }
    }
}
