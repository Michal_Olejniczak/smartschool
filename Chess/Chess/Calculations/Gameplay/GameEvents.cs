﻿using Chess.Calculations.Mappers;
using Chess.Object;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Chess.Calculations.Gameplay
{
    public static class GameEvents
    {
        private static Chessboard _chessboard;
        public static bool GameStarted { get { return (_chessboard != null ? true : false); } }
        private static Teams _currentTeam;
        private static DispatcherTimer _whiteTimer;
        private static DispatcherTimer _blackTimer;
        private static TimeSpan _whiteTime;
        private static TimeSpan _blackTime;
        private static bool _playerVsPlayer;

        public static void StartGame(bool playerPlayer)
        {
            _chessboard = new Chessboard();
            _whiteTimer = new DispatcherTimer();
            _blackTimer = new DispatcherTimer();
            _whiteTimer.Tick += whiteTimer_Tick;
            _blackTimer.Tick += blackTimer_Tick;
            _whiteTimer.Interval = new TimeSpan(0, 0, 1);
            _blackTimer.Interval = new TimeSpan(0, 0, 1);
            _whiteTime = new TimeSpan(0, 0, 0);
            _blackTime = new TimeSpan(0, 0, 0);
            _playerVsPlayer = playerPlayer;
            GraphicsDisp.SetWhiteTimer(_whiteTime);
            GraphicsDisp.SetBlackTimer(_blackTime);

            GraphicsDisp.PrepeareChessboard();

            foreach (Teams team in Enum.GetValues(typeof(Teams)))
            {
                if (team == Teams.White)
                {
                    for (int i = 6; i <= 62; i = i + 8)
                    {
                        Pawn pawn = new Pawn(team, (byte)i);
                        _chessboard.AddChessman(pawn);
                    }
                    for (int i = 7; i <= 63; i = i + 56)
                    {
                        Rook rook = new Rook(team, (byte)i);
                        _chessboard.AddChessman(rook);
                    }
                    for (int i = 15; i <= 55; i = i + 40)
                    {
                        Knight knight = new Knight(team, (byte)i);
                        _chessboard.AddChessman(knight);
                    }
                    for (int i = 23; i <= 47; i = i + 24)
                    {
                        Bishop bishop = new Bishop(team, (byte)i);
                        _chessboard.AddChessman(bishop);
                    }
                    Queen queen = new Queen(team, 31);
                    _chessboard.AddChessman(queen);
                    King king = new King(team, 39);
                    _chessboard.AddChessman(king);
                }
                else
                {
                    for (int i = 1; i <= 57; i = i + 8)
                    {
                        Pawn pawn = new Pawn(team, (byte)i);
                        _chessboard.AddChessman(pawn);
                    }
                    for (int i = 0; i <= 56; i = i + 56)
                    {
                        Rook rook = new Rook(team, (byte)i);
                        _chessboard.AddChessman(rook);
                    }
                    for (int i = 8; i <= 48; i = i + 40)
                    {
                        Knight knight = new Knight(team, (byte)i);
                        _chessboard.AddChessman(knight);
                    }
                    for (int i = 16; i <= 40; i = i + 24)
                    {
                        Bishop bishop = new Bishop(team, (byte)i);
                        _chessboard.AddChessman(bishop);
                    }
                    Queen queen = new Queen(team, 24);
                    _chessboard.AddChessman(queen);
                    King king = new King(team, 32);
                    _chessboard.AddChessman(king);
                }
            }
            _currentTeam = Teams.White;
            GraphicsDisp.SetCurrentTeam(_currentTeam.ToString());
            _whiteTimer.Start();
        }

        public static void ShowPossibleMove(Button btn)
        {
            byte location = BtnMapper.GetLocationFromBtn(btn);
            _chessboard.ShowPossibleMoves(location, _currentTeam);
        }

        public static void PerformMove(Button btn)
        {
            byte location = BtnMapper.GetLocationFromBtn(btn);
            if (!_playerVsPlayer)
            {
                if (_chessboard.StartMove(location))
                {
                    if (_currentTeam == Teams.White)
                    {
                        _currentTeam = Teams.Black;
                        _whiteTimer.Stop();
                        _blackTimer.Start();
                    }
                    else
                    {
                        _currentTeam = Teams.White;
                        _whiteTimer.Start();
                        _blackTimer.Stop();
                    }
                }
                Chessboard.UnhigliteBtn();
                GraphicsDisp.SetCurrentTeam(_currentTeam.ToString());
                _chessboard.AfterMove();
            }
            else
            {
                if (_chessboard.StartMove(location))
                {
                    Chessboard.UnhigliteBtn();
                    GraphicsDisp.SetCurrentTeam(_currentTeam.ToString());
                    _chessboard.AfterMove();
                    _currentTeam = Teams.Black;
                    _whiteTimer.Stop();
                    _blackTimer.Start();
                    StartCPUMoveCalculation();
                    _currentTeam = Teams.White;
                    _whiteTimer.Start();
                    _blackTimer.Stop();
                }
                Chessboard.UnhigliteBtn();
            }
        }

        private static void whiteTimer_Tick(object sender, EventArgs e)
        {
            _whiteTime = _whiteTime.Add(new TimeSpan(0, 0, 1));
            GraphicsDisp.SetWhiteTimer(_whiteTime);
        }

        private static void blackTimer_Tick(object sender, EventArgs e)
        {
            _blackTime = _blackTime.Add(new TimeSpan(0, 0, 1));
            GraphicsDisp.SetBlackTimer(_blackTime);
        }

        public static void Dispose()
        {
            if (_whiteTimer != null)
            {
                _whiteTimer.Stop();
                _whiteTimer = null;
            }
            if (_blackTimer != null)
            {
                _blackTimer.Stop();
                _blackTimer = null;
            }
        }

        public static Teams CurrentTeam { get { return _currentTeam; } }

        public static void GameEnd(bool restart = false, bool winner = true)
        {
            GraphicsDisp.UnhiglightBtns();
            if (_currentTeam == Teams.White)
                _whiteTimer.Stop();
            else
                _blackTimer.Stop();

            if (!restart)
            {
                if (winner)
                    MainWindow.main.ThrowGameEnd = Teams.White.ToString();
                else
                    MainWindow.main.ThrowGameEnd = "";
            }
        }

        public static void Undo()
        {
            if (!_playerVsPlayer)
            {
                if (!_chessboard.Undo())
                    return;
                if (_currentTeam == Teams.White)
                {
                    _currentTeam = Teams.Black;
                    _whiteTimer.Stop();
                    _blackTimer.Start();
                }
                else
                {
                    _currentTeam = Teams.White;
                    _whiteTimer.Start();
                    _blackTimer.Stop();
                }
            }
            else
            {
                if (!_chessboard.Undo())
                    return;
                if (!_chessboard.Undo())
                    return;
            }
            Chessboard.UnhigliteBtn();
            GraphicsDisp.SetCurrentTeam(_currentTeam.ToString());
        }

        private static void StartCPUMoveCalculation()
        {
            CPUPlayer.StartCPUMove(_chessboard);
        }
    }
}
