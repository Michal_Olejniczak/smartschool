﻿using Chess.Calculations.Mappers;
using Chess.Object;
using System;
using System.Text;
using System.Windows.Controls;
using System.Drawing;
using System.Linq;
using System.Windows.Media;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace Chess.Calculations.Gameplay
{
    public static class GraphicsDisp
    {
        private static SolidColorBrush _whiteField =
            new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 255, 0));
        private static SolidColorBrush _blackField =
            new SolidColorBrush(System.Windows.Media.Color.FromRgb(100, 100, 100));
        private static SolidColorBrush _higlated =
            new SolidColorBrush(System.Windows.Media.Color.FromRgb(100, 0, 100));
        private static SolidColorBrush _capture =
            new SolidColorBrush(System.Windows.Media.Color.FromRgb(0, 100, 100));

        private static Dictionary<byte, System.Windows.Media.Brush> _higlitedBtns
            = new Dictionary<byte, System.Windows.Media.Brush>();

        public static void PrepeareChessboard()
        {
            int j = 0;
            for (int i = 0; i <= 63; i = i + 2)
            {
                Button btn = BtnMapper.GetBtnFromloaction((byte)i);
                btn.Background = _whiteField;
                btn.Content = "";
                if ((i + 1) % 8 == 0 || (i + 2) % 8 == 0)
                {
                    if (j % 2 == 0)
                        i++;
                    else
                        i--;
                    j++;
                }
            }
            j = 0;
            for (int i = 1; i <= 63; i = i + 2)
            {
                Button btn = BtnMapper.GetBtnFromloaction((byte)i);
                btn.Background = _blackField;
                btn.Content = "";
                if ((i + 1) % 8 == 0 || (i + 2) % 8 == 0)
                {
                    if (j % 2 == 0)
                        i--;
                    else
                        i++;
                    j++;
                }
            }
        }

        public static void ChangeChessmanDisplay(Chessman chessman)
        {
            Button btn = BtnMapper.GetBtnFromloaction(chessman.Location);
            if (chessman.HistoryLocations.Count > 0)
            {
                Button btnLast = BtnMapper.GetBtnFromloaction(
                    chessman.HistoryLocations[chessman.HistoryLocations.Keys.Max()]);
                btnLast.Content = "";
            }

            string str = "Resources/" + string.Format(chessman.ToString()).ToLower() + ".png";

            btn.Content = new System.Windows.Controls.Image
            {
                Source = new BitmapImage(new Uri(str, UriKind.Relative)),
                VerticalAlignment = System.Windows.VerticalAlignment.Stretch,
                HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch
            };
        }

        public static void HiglightBtnToMove(byte locationToHiglight)
        {
            Button btn = BtnMapper.GetBtnFromloaction(locationToHiglight);
            if (!_higlitedBtns.ContainsKey(locationToHiglight))
                _higlitedBtns.Add(locationToHiglight, btn.Background);
            btn.Background = _higlated;
        }

        public static void HiglightBtnToCapture(byte locationToHiglight)
        {
            Button btn = BtnMapper.GetBtnFromloaction(locationToHiglight);
            if (!_higlitedBtns.ContainsKey(locationToHiglight))
                _higlitedBtns.Add(locationToHiglight, btn.Background);
            btn.Background = _capture;
        }

        public static void UnhiglightBtns()
        {
            
            foreach(KeyValuePair<byte, System.Windows.Media.Brush> 
                higlited in _higlitedBtns)
            {
                Button btn = BtnMapper.GetBtnFromloaction(higlited.Key);
                btn.Background = higlited.Value;
            }
            _higlitedBtns.Clear();
        }

        public static void RemoveChessmanDisplay(byte locationToClear)
        {
            Button btn = BtnMapper.GetBtnFromloaction(locationToClear);
            btn.Content = "";
        }

        public static void SetCurrentTeam(string team)
        {
            MainWindow.main.CurrentPlayerLblContent = "Current player is " + team;
        }

        public static void SetWhiteTimer(TimeSpan timer)
        {
            MainWindow.main.WhiteTimerLblContent = string.Format("White time is {0:HH:mm:ss}", timer.ToString());
        }

        public static void SetBlackTimer(TimeSpan timer)
        {
            MainWindow.main.BlackTimerLblContent = string.Format("Black time is {0:HH:mm:ss}", timer.ToString());
        }

        public static void SetWinner(string team)
        {
            MainWindow.main.CurrentPlayerLblContent = "Winner is " + team;
        }

        public static void SetCheckedTeam(string team)
        {
            MainWindow.main.CheckLblContent = string.Format((team.Length > 0) ? team + " is checked!" : "");
        }
    }
}
