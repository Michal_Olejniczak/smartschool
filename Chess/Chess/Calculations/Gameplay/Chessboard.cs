﻿using Chess.Forms;
using Chess.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Chess.Calculations.Gameplay
{
    public class Chessboard
    {
        private Chessman[] _chessboard = new Chessman[64];
        private List<Chessman> _capturedChessman;
        private int _move;
        private Chessman _selectedChessman;
        private Teams? _checkedTeam;
        private string _pawnTransform;
        private bool _checkForCheckAfterMove = false;

        internal static Chessboard chess;

        /// <summary>
        /// Pobranie wartości wybranej w oknie zminay pionka w inną figurę
        /// </summary>
        internal string PawnTransform
        {
            set { Dispatcher.CurrentDispatcher.Invoke(new Action(() => { _pawnTransform = value; })); }
        }

        public int Move { get { return _move; } }
        public Chessman[] ChessboardView { get { return _chessboard; } }

        /// <summary>
        /// Konstruktor
        /// </summary>
        public Chessboard()
        {
            _move = 1;
            _capturedChessman = new List<Chessman>();
            _checkedTeam = null;
            chess = this;
        }

        /// <summary>
        /// Dodanie figury na planszę
        /// </summary>
        /// <param name="chessman"></param>
        public void AddChessman(Chessman chessman)
        {
            _chessboard[chessman.Location] = chessman;
            _chessboard[chessman.Location].HistoryLocations.Add(0, chessman.Location);
            GraphicsDisp.ChangeChessmanDisplay(chessman);
        }

        /// <summary>
        /// Czy pole jest zajęte?
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public Chessman IsFieldTaken(byte field)
        {
            return _chessboard[field];
        }

        /// <summary>
        /// Rozpoczęcie ruchu figury
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public bool StartMove(byte location)
        {
            bool moved = MoveChessmanIntoField(_selectedChessman, location, true);
            _selectedChessman = null;
            return moved;
        }

        /// <summary>
        /// Wybranie akcji w zależności od wyniku końcowego ruchu
        /// </summary>
        /// <param name="chessman"></param>
        /// <param name="finishField"></param>
        /// <param name="performMove"></param>
        /// <returns></returns>
        private bool MoveChessmanIntoField(Chessman chessman, byte finishField,
                                          bool performMove = false)
        {
            if (chessman == null) { return false; }

            byte startField = chessman.Location;

            if (startField == finishField || startField > 63 || finishField > 63) { return false; }
            if (IsFieldTaken(startField) != chessman) { return false; }

            Chessman targetField = IsFieldTaken(finishField);

            switch (IsMovePossible(chessman, finishField))
            {
                case MovesFinish.Move:
                    if (performMove)
                        PerformMove(chessman, finishField);
                    return true;
                case MovesFinish.Capture:
                    if (performMove)
                        PerformCapture(chessman, targetField);
                    return true;
                default:
                    return MoveChessmanIntoUnregularField(chessman, finishField, performMove);
            }
        }

        private bool MoveChessmanIntoUnregularField(Chessman chessman, byte finishField,
                                          bool performMove = false)
        {
            Chessman targetField = IsFieldTaken(finishField);
            if (chessman.GetType() == typeof(Pawn))
            {
                byte[] pawnMoves = CheckPawnCapture(chessman);
                for (int i = 0; i <= 1; i++)
                {
                    if (pawnMoves[i] != finishField)
                        continue;
                    if (performMove)
                    {
                        if (targetField != null)
                            PerformCapture(chessman, _chessboard[pawnMoves[i]]);
                        else
                            PerformCapture(chessman, _chessboard[pawnMoves[i + 2]], pawnMoves[i]);
                    }
                    return true;
                }
            }
            else if (chessman.GetType() == typeof(King) &&
                    chessman.HistoryLocations.Count <= 1)
            {
                foreach (Chessman c in _chessboard)
                {
                    if (c == null)
                        continue;
                    if (c.Team == chessman.Team && c.GetType() == typeof(Rook)
                        && c.HistoryLocations.Count <= 1)
                        return Castling(chessman, c, true);
                }
            }
            else if (chessman.GetType() == typeof(Rook) &&
                chessman.HistoryLocations.Count <= 1)
            {
                foreach (Chessman c in _chessboard)
                {
                    if (c == null)
                        continue;
                    if (c.Team == chessman.Team && c.GetType() == typeof(King)
                        && c.HistoryLocations.Count <= 1)
                        return Castling(chessman, c, true);
                }
            }
            return false;
        }

        /// <summary>
        /// Wykonanie roszady
        /// </summary>
        /// <param name="chessman1"></param>
        /// <param name="chessman2"></param>
        /// <param name="performMove"></param>
        /// <returns></returns>
        private bool Castling(Chessman chessman1, Chessman chessman2, bool performMove)
        {
            Chessman king = null;
            Chessman rook = null;

            if (chessman1.GetType() == typeof(King))
                king = chessman1;
            else if (chessman1.GetType() == typeof(Rook))
                rook = chessman1;
            else
                return false;

            if (chessman2.GetType() == typeof(King) && king == null)
                king = chessman2;
            else if (chessman2.GetType() == typeof(Rook) && rook == null)
                rook = chessman2;
            else
                return false;

            if (king.HistoryLocations.Count() > 1 || rook.HistoryLocations.Count() > 1)
                return false;

            byte minVal = Math.Min(king.Location, rook.Location);
            byte maxVal = Math.Max(king.Location, rook.Location);

            for (int i = minVal + 8; i < maxVal; i = i + 8)
            {
                if (_chessboard[i] != null)
                    return false;
            }

            if (performMove)
            {
                if (king.Location == minVal)
                {
                    PerformMove(king, (byte)(king.Location + 16));
                    PerformMove(rook, (byte)(rook.Location - 16));
                }
                else
                {
                    PerformMove(king, (byte)(king.Location - 16));
                    PerformMove(rook, (byte)(rook.Location + 24));
                }
            }
            return true;
        }

        /// <summary>
        /// Wykonanie ruchu
        /// </summary>
        /// <param name="chessman"></param>
        /// <param name="finishField"></param>
        private void PerformMove(Chessman chessman, byte finishField)
        {
            _checkForCheckAfterMove = (_checkedTeam == null) ? false : true;
            byte startField = chessman.Location;
            chessman.Location = finishField;
            GraphicsDisp.ChangeChessmanDisplay(chessman);
            chessman.AddPreviousLocation(_move, finishField);
            Array.Copy(_chessboard, startField, _chessboard, finishField, 1);
            if (_chessboard[startField].Id == _chessboard[finishField].Id)
                _chessboard[startField] = null;
            chessman.AfterMove();
            PawnPromotionCheck(chessman);
            if (!CheckIsCheck((GameEvents.CurrentTeam == Teams.Black) ? Teams.White : Teams.Black))
            {
                if (CheckDraw((GameEvents.CurrentTeam == Teams.Black) ? Teams.White : Teams.Black))
                    GameEnd(false);
                else
                    _move++;
            }
        }

        /// <summary>
        /// Wykonanie bicia
        /// </summary>
        /// <param name="chessman"></param>
        /// <param name="capturedChessman"></param>
        /// <param name="chessmanLoc"></param>
        private void PerformCapture(Chessman chessman, Chessman capturedChessman,
            byte chessmanLoc = 100)
        {
            if (chessman.Team == capturedChessman.Team)
                return;

            if (capturedChessman.GetType() == typeof(King))
                return;

            byte finishField = capturedChessman.Location;
            _chessboard[capturedChessman.Location].AddPreviousLocation(_move, 100);
            _chessboard[capturedChessman.Location].Captured = true;

            _capturedChessman.Add(_chessboard[finishField]);

            _chessboard[capturedChessman.Location] = null;
            PerformMove(chessman, (chessmanLoc == 100) ? finishField : chessmanLoc);
            if (chessmanLoc != 100)
                GraphicsDisp.RemoveChessmanDisplay(finishField);
        }

        /// <summary>
        /// Podświetlenie pól do roszady
        /// </summary>
        /// <param name="chessman"></param>
        private void ShowCastling(Chessman chessman)
        {
            if (chessman.GetType() == typeof(King) &&
                chessman.HistoryLocations.Count <= 1)
            {
                try
                {
                    foreach (Chessman c in _chessboard.Where(i => i != null)
                        .Where(i => i.Team == chessman.Team &&
                        i.GetType() == typeof(Rook) &&
                        i.HistoryLocations.Count <= 1))
                    {
                        if (Castling(chessman, c, false))
                            GraphicsDisp.HiglightBtnToCapture(c.Location);
                    }
                }
                catch { } // nie ma już wieży
            }
            else if (chessman.GetType() == typeof(Rook) &&
                chessman.HistoryLocations.Count <= 1)
            {
                Chessman c = _chessboard.Where(i => i != null)
                    .FirstOrDefault(i => i.Team == chessman.Team &&
                    i.GetType() == typeof(King));

                if (c.HistoryLocations.Count <= 1)
                {
                    if (Castling(chessman, c, false))
                        GraphicsDisp.HiglightBtnToCapture(c.Location);
                }
            }
        }

        /// <summary>
        /// Wyświetlenie dopuszczalnych ruchów
        /// </summary>
        /// <param name="location"></param>
        /// <param name="currentTeam"></param>
        public void ShowPossibleMoves(byte location, Teams currentTeam)
        {
            Chessman chess = _chessboard[location];
            if (chess == null)
                return;
            if (chess.Team != currentTeam)
                return;
            _selectedChessman = chess;
            if (chess.GetType() == typeof(Pawn))
            {
                foreach (byte f in CheckPawnCapture(chess))
                {
                    if (f < 64)
                        GraphicsDisp.HiglightBtnToCapture(f);
                }
            }
            ShowCastling(chess);

            foreach (sbyte move in chess.AllowedMove)
            {
                byte newLocation = (byte)(chess.Location + move);
                if (chess.OneMove)
                    HiglightBtnsBaseOnMoveType(
                        IsMovePossible(chess, newLocation), newLocation);
                else
                {
                    MovesFinish? isMoveValid = IsMovePossible(chess, newLocation);
                    while (isMoveValid != null)
                    {
                        HiglightBtnsBaseOnMoveType(isMoveValid, newLocation);
                        if (isMoveValid != MovesFinish.Move && isMoveValid != MovesFinish.Check)
                            break;
                        newLocation = (byte)(newLocation + move);
                        isMoveValid = IsMovePossible(chess, newLocation);
                    }
                }
            }
        }

        /// <summary>
        /// Wybranie rodzaju podświetlenia w zależności od rodzaju ruchu
        /// </summary>
        /// <param name="isMoveValid"></param>
        /// <param name="newLocation"></param>
        private void HiglightBtnsBaseOnMoveType(MovesFinish? isMoveValid, byte newLocation)
        {
            switch (isMoveValid)
            {
                case MovesFinish.Capture:
                    GraphicsDisp.HiglightBtnToCapture(newLocation);
                    break;
                case MovesFinish.Castling:
                    GraphicsDisp.HiglightBtnToCapture(newLocation);
                    break;
                case MovesFinish.Move:
                    GraphicsDisp.HiglightBtnToMove(newLocation);
                    break;
            }
        }

        /// <summary>
        /// Początek sprawdzenia czy ruch jest możliwy
        /// </summary>
        /// <param name="chessman"></param>
        /// <param name="newLocation"></param>
        /// <param name="ignoreChessman"></param>
        /// <returns></returns>
        private MovesFinish? IsMovePossible(Chessman chessman,
            byte newLocation, Chessman ignoreChessman = null)
        {
            if (chessman.OneMove)
                return IsMovePossibleForOneMove(chessman, newLocation);
            else
                return IsMovePossibleFonContMove(chessman, newLocation, ignoreChessman);
        }

        /// <summary>
        /// Czy ruch jest możliwy dla figur o stałym ruchu (Pionek, skoczek, król)
        /// </summary>
        /// <param name="chessman"></param>
        /// <param name="newLocation"></param>
        /// <returns></returns>
        private MovesFinish? IsMovePossibleForOneMove(Chessman chessman,
            byte newLocation)
        {
            foreach (sbyte m in chessman.AllowedMove)
            {
                if (!IsMoveContinous(chessman.Location, newLocation))
                    continue;
                if (chessman.Location + m != newLocation)
                    continue;
                if (chessman.GetType() == typeof(Knight))
                {
                    if (newLocation % 8 > 2 + (chessman.Location % 8) ||
                        newLocation % 8 < (chessman.Location % 8) - 2)
                        continue;
                }
                else if (chessman.GetType() == typeof(King))
                {
                    if (CheckIsCheck(chessman.Team, newLocation))
                        return null;
                }
                if (_chessboard[newLocation] == null)
                {
                    if (chessman.GetType() == typeof(Pawn))
                    {
                        if (Math.Abs(m) == 2)
                        {
                            if (_chessboard[chessman.Location + m / 2] != null)
                                return null;
                        }
                    }
                    return MovesFinish.Move;
                }
                else
                {
                    if (_chessboard[newLocation].Team != chessman.Team)
                    {
                        if (_chessboard[newLocation].GetType() == typeof(King))
                            return MovesFinish.Check;
                        if (chessman.GetType() != typeof(Pawn))
                            return MovesFinish.Capture;
                    }
                    else
                        return MovesFinish.Protection;
                }
            }
            return null;
        }

        /// <summary>
        /// Czy ruch jest możliwy dla figur o ciągłym ruchu
        /// </summary>
        /// <param name="chessman"></param>
        /// <param name="newLocation"></param>
        /// <param name="ignoreChessman"></param>
        /// <returns></returns>
        private MovesFinish? IsMovePossibleFonContMove(Chessman chessman,
            byte newLocation, Chessman ignoreChessman)
        {
            sbyte move = ChessmanAllowedMoveOnMove(chessman, newLocation);
            if (move == 0)
                return null;

            Moves? moveTypeTemp = GetMoveFromVector(move);
            Moves moveType;
            if (moveTypeTemp == null)
                return null;
            else
                moveType = (Moves)moveTypeTemp;

            if (!IsMoveInsideBounds(chessman, newLocation, moveType))
                return null;

            for (int i = chessman.Location + move;
                (move < 0) ? i > newLocation : i < newLocation; i = i + move)
            {
                if (!IsMoveInsideBounds(chessman, (byte)i, moveType))
                    return null;
                if (ignoreChessman == null)
                {
                    if (_chessboard[i] != null)
                        return null;
                }
                else
                {
                    if (_chessboard[i] != null)
                    {
                        if (_chessboard[i] != ignoreChessman)
                            return null;
                    }
                }
            }

            if (_chessboard[newLocation] == null)
                return MovesFinish.Move;
            else if (_chessboard[newLocation].Team == chessman.Team)
                return MovesFinish.Protection;
            else
            {
                if (_chessboard[newLocation].GetType() == typeof(King))
                    return MovesFinish.Check;
                return MovesFinish.Capture;
            }
        }

        /// <summary>
        /// Sprawdzenie czy wybrane położenie jest dopuszczalne dla figur o stałym ruchu
        /// </summary>
        /// <param name="chessman"></param>
        /// <param name="newLocation"></param>
        /// <returns></returns>
        private sbyte ChessmanAllowedMoveOnMove(Chessman chessman, byte newLocation)
        {
            foreach (sbyte m in chessman.AllowedMove)
            {
                if ((newLocation - chessman.Location) % m != 0)
                    continue;
                if (Math.Sign(newLocation - chessman.Location) != Math.Sign(m))
                    continue;
                if (Math.Abs(m) == 1)
                {
                    if (Math.Floor((double)(chessman.Location / 8)) !=
                        Math.Floor((double)(newLocation / 8)))
                        continue;
                }
                return m;
            }
            return 0;
        }

        /// <summary>
        /// Pobranie rodzaju ruchu w zależności od przemieszczenia
        /// </summary>
        /// <param name="move"></param>
        /// <returns></returns>
        private Moves? GetMoveFromVector(sbyte move)
        {
            switch (Math.Abs(move))
            {
                case 1:
                    return Moves.Vertical;
                case 8:
                    return Moves.Horizontal;
                default:
                    switch (move)
                    {
                        case -9:
                            return Moves.DiagonalUp;
                        case 7:
                            return Moves.DiagonalUp;
                        case -7:
                            return Moves.DiagonalDown;
                        case 9:
                            return Moves.DiagonalDown;
                        default:
                            return null;
                    }
            }
        }

        /// <summary>
        /// Sprawdzenie czy ciągły ruch nie wychodzi poza planszę
        /// </summary>
        /// <param name="chessman"></param>
        /// <param name="location"></param>
        /// <param name="moveType"></param>
        /// <returns></returns>
        private bool IsMoveInsideBounds(Chessman chessman, byte location, Moves moveType)
        {
            if (!IsInBoardBounds(location))
                return false;
            switch (moveType)
            {
                case Moves.DiagonalUp:
                    if (location % 8 == 7)
                        return false;
                    break;
                case Moves.DiagonalDown:
                    if (location % 8 == 0)
                        return false;
                    break;
                case Moves.Vertical:
                    if (Math.Floor((decimal)(chessman.Location / 8))
                        != Math.Floor((decimal)(location / 8)))
                        return false;
                    break;
                case Moves.Horizontal:
                    return true;
                default:
                    return true;
            }
            return true;
        }

        /// <summary>
        /// Wyłączenie podświetlenia pól
        /// </summary>
        public static void UnhigliteBtn()
        {
            GraphicsDisp.UnhiglightBtns();
        }

        /// <summary>
        /// Sprawdzenie czy figury o stałym ruchu nie wychodzą za planszę
        /// </summary>
        /// <param name="startLoc"></param>
        /// <param name="targetLoc"></param>
        /// <returns></returns>
        private bool IsMoveContinous(byte startLoc, byte targetLoc)
        {
            if (!IsInBoardBounds(targetLoc))
                return false;
            if (startLoc % 8 == 0 && targetLoc % 8 == 7)
                return false;
            if (startLoc % 8 == 7 && targetLoc % 8 == 0)
                return false;

            return true;
        }

        /// <summary>
        /// Sprawdzenie dopuszczalnych bić pionka
        /// </summary>
        /// <param name="chessman"></param>
        /// <returns></returns>
        private byte[] CheckPawnCapture(Chessman chessman)
        {
            byte[] fields = new byte[4] { 100, 100, 100, 100 };

            int chkLoc1 = chessman.Location + (9 * chessman.AllowedMove[0]);
            int chkLoc2 = chessman.Location + (-7 * chessman.AllowedMove[0]);
            int chkLoc3 = chessman.Location + 8;
            int chkLoc4 = chessman.Location - 8;

            if (IsInBoardBounds(chkLoc1))
            {
                if (_chessboard[chkLoc1] != null)
                {
                    if (_chessboard[chkLoc1].Team != chessman.Team)
                    {
                        if (_chessboard[chkLoc1].GetType() != typeof(King))
                            fields[0] = (byte)chkLoc1;
                    }
                }
                Chessman enPassant = _chessboard[(chessman.Team == Teams.Black) ? chkLoc3 : chkLoc4];
                if (_chessboard[chkLoc1] == null && enPassant != null)
                {
                    if (chessman.Team != enPassant.Team && CheckenPassant(enPassant))
                    {
                        fields[2] = (byte)((chessman.Team == Teams.Black) ? chkLoc3 : chkLoc4);
                        fields[0] = (byte)chkLoc1;
                    }
                }
            }

            if (IsInBoardBounds(chkLoc2))
            {
                if (_chessboard[chkLoc2] != null)
                {
                    if (_chessboard[chkLoc2].Team != chessman.Team)
                    {
                        if (_chessboard[chkLoc2].GetType() != typeof(King))
                            fields[1] = (byte)chkLoc2;
                    }
                }
                Chessman enPassant = _chessboard[(chessman.Team == Teams.Black) ? chkLoc4 : chkLoc3];
                if (_chessboard[chkLoc2] == null && enPassant != null)
                {
                    if (enPassant.Team != chessman.Team && CheckenPassant(enPassant))
                    {
                        fields[3] = (byte)((chessman.Team == Teams.Black) ? chkLoc4 : chkLoc3);
                        fields[1] = (byte)chkLoc2;
                    }
                }
            }

            return fields;
        }

        /// <summary>
        /// Sprawdzenie bicia w przelocie
        /// </summary>
        /// <param name="chessman"></param>
        /// <returns></returns>
        private bool CheckenPassant(Chessman chessman)
        {
            if (chessman.GetType() != typeof(Pawn)) //mozna zbić tylko pionka
                return false;
            if (chessman.HistoryLocations.Count() != 2) //musi ruszyć się tylko raz
                return false;
            if (Math.Abs(chessman.HistoryLocations.ElementAt(1).Value -
                chessman.HistoryLocations.ElementAt(0).Value) != 2) // musi ruszyć się o 2
                return false;
            if (chessman.HistoryLocations.Keys.Max() != _move - 1) // musi zrobić to w poprzedniej rundzie
                return false;
            return true;
        }

        /// <summary>
        /// Sprawdzenie czy jesteśmy w granicy tablicy
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private bool IsInBoardBounds(int number)
        {
            if (number >= 0 && number <= 63)
                return true;
            return false;
        }

        /// <summary>
        /// Sprawdzenie czy jest szach
        /// </summary>
        /// <param name="checkTeam"></param>
        /// <param name="checkField"></param>
        /// <returns></returns>
        private bool CheckIsCheck(Teams checkTeam, byte checkField = 100)
        {
            bool performAction = checkField == 100 ? true : false;
            Teams checkingTeam = (checkTeam == Teams.Black) ? Teams.White : Teams.Black;
            Chessman targetKing = _chessboard.Where(i => i != null)
                    .FirstOrDefault(i => i.Team == checkTeam && i.GetType() == typeof(King));
            if (performAction)
                checkField = targetKing.Location;
            foreach (Chessman chessman in _chessboard.Where(i => i != null)
                .Where(i => i.Team == checkingTeam))
            {
                if (chessman.GetType() != typeof(Pawn))
                {
                    MovesFinish? chessmanMove = IsMovePossible(chessman, checkField, targetKing);
                    if (performAction)
                    {
                        if (chessmanMove == MovesFinish.Check)
                        {
                            _checkedTeam = checkTeam;
                            CheckIsCheckmate(chessman);
                            GraphicsDisp.SetCheckedTeam(checkTeam.ToString());
                            return true;
                        }
                    }
                    else
                    {
                        if (chessmanMove == MovesFinish.Move || chessmanMove == MovesFinish.Protection)
                            return true;
                    }
                }
                else
                {
                    int chkLoc1 = chessman.Location + (9 * chessman.AllowedMove[0]);
                    int chkLoc2 = chessman.Location + (-7 * chessman.AllowedMove[0]);
                    if (chkLoc1 == checkField || chkLoc2 == checkField)
                    {
                        if (performAction)
                        {
                            _checkedTeam = checkTeam;
                            GraphicsDisp.SetCheckedTeam(checkTeam.ToString());
                            CheckIsCheckmate(chessman);
                        }
                        return true;
                    }
                }
            }
            if (performAction)
            {
                _checkedTeam = null;
                GraphicsDisp.SetCheckedTeam("");
            }
            return false;
        }

        /// <summary>
        /// Sprawdzenie czy szach mat
        /// </summary>
        /// <param name="checkingChessman"></param>
        /// <returns></returns>
        private bool CheckIsCheckmate(Chessman checkingChessman)
        {
            Chessman checkedKing =
                _chessboard.Where(i => i != null)
                .FirstOrDefault(i => i.GetType() == typeof(King) &&
                i.Team != checkingChessman.Team);

            // Czy król może się ruszyć na inne pole?
            if (CanCheckedKingMove(checkedKing))
                return false;

            // Ktoś może zbić szachującego?
            if (CanChekingChessmanCapture(checkingChessman))
                return false;

            //Ktoś może stanąć na lini szachu?
            if (CanCheckInterrupt(checkedKing, checkingChessman))
                return false;

            GameEnd();
            return true;
        }

        /// <summary>
        /// Sprawdzenie czy król się może ruszyć
        /// </summary>
        /// <param name="checkedKing"></param>
        /// <returns></returns>
        private bool CanCheckedKingMove(Chessman checkedKing)
        {
            foreach (byte m in checkedKing.AllowedMove)
            {
                if (IsMovePossible(checkedKing, (byte)(checkedKing.Location + m))
                    == MovesFinish.Move)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Sprawdzenie czy można zbić daną figurę
        /// </summary>
        /// <param name="checkedKing"></param>
        /// <param name="checkingChessman"></param>
        /// <returns></returns>
        private bool CanChekingChessmanCapture(Chessman checkingChessman)
        {
            foreach (Chessman checkedPlayerChessman in _chessboard
                .Where(i => i != null)
                .Where(i => i.Team != checkingChessman.Team))
            {
                if (IsMovePossible(checkedPlayerChessman, checkingChessman.Location)
                    == MovesFinish.Capture)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Czy któraś z figur może stanąc na drodze szachu
        /// </summary>
        /// <param name="checkedKing"></param>
        /// <param name="checkingChessman"></param>
        /// <returns></returns>
        private bool CanCheckInterrupt(Chessman checkedKing, Chessman checkingChessman)
        {
            if (checkingChessman.OneMove) // musi byc na dystans
                return false;

            // pola pomiedzy 
            byte[] freeFields = FieldsBetweenChessmans(checkingChessman, checkedKing);

            foreach (byte f in freeFields)
            {
                foreach (Chessman saveChessman in _chessboard
                    .Where(i => i != null)
                    .Where(i => i.Team == checkedKing.Team))
                {
                    if (IsMovePossible(saveChessman, f) == MovesFinish.Move)
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Wyliczenie pól pomiędzy dwoma figurami
        /// </summary>
        /// <param name="attacker"></param>
        /// <param name="captured"></param>
        /// <returns></returns>
        private byte[] FieldsBetweenChessmans(Chessman attacker, Chessman captured)
        {
            byte[] output = new byte[1] { 100 };

            foreach (sbyte m in attacker.AllowedMove)
            {
                if ((captured.Location - attacker.Location) % m != 0)
                    continue;
                if (Math.Sign(captured.Location - attacker.Location) != Math.Sign(m))
                    continue;
                if (Math.Abs(m) == 1)
                {
                    if (Math.Floor((double)(attacker.Location / 8)) !=
                        Math.Floor((double)(captured.Location / 8)))
                        continue;
                }
                int j = 0;
                for (int i = attacker.Location + m;
                        (m < 0) ? i > captured.Location : i < captured.Location; i = i + m)
                {
                    output[j] = (byte)i;
                    Array.Resize(ref output, output.Length + 1);
                    j++;
                }
                break;
            }
            Array.Resize(ref output, output.Length - 1);
            return output;
        }

        /// <summary>
        /// Sprawdzenie czy jest remis
        /// </summary>
        /// <param name="team"></param>
        /// <returns></returns>
        private bool CheckDraw(Teams team)
        {
            Chessman King =
                _chessboard.Where(i => i != null)
                .FirstOrDefault(i => i.GetType() == typeof(King) &&
                i.Team == team);
            foreach (sbyte m in King.AllowedMove)
            {
                MovesFinish? mType = IsMovePossible(King, (byte)(King.Location + m));
                if (mType == MovesFinish.Capture || mType == MovesFinish.Move)
                    return false;
            }
            if (_chessboard.Where(i => i != null)
                .Where(i => i.Team == team &&
                i.GetType() != typeof(Pawn) &&
                i.GetType() != typeof(King)).Count() > 0)
                return false;
            foreach (Chessman pawn in _chessboard.Where(i => i != null)
                .Where(i => i.GetType() == typeof(Pawn) && i.Team == team))
            {
                foreach (sbyte m in pawn.AllowedMove)
                {
                    MovesFinish? mType = IsMovePossible(pawn, (byte)(pawn.Location + m));
                    if (mType == MovesFinish.Capture || mType == MovesFinish.Move)
                        return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Zakończenie gry
        /// </summary>
        /// <param name="winner"></param>
        private void GameEnd(bool winner = true)
        {
            GameEvents.GameEnd(false, winner);
        }

        /// <summary>
        /// Cofanie
        /// </summary>
        /// <returns></returns>
        public bool Undo()
        {
            if (_move == 1)
                return false;
            _move--;
            Teams prevTeam = GameEvents.CurrentTeam == Teams.Black ? Teams.White : Teams.Black;
            Chessman chessman = _chessboard.Where(i => i != null)
                .FirstOrDefault(i => i.HistoryLocations.ContainsKey(_move));

            byte currField = chessman.Location;

            if (chessman.FromPromotion && chessman.HistoryLocations.Count == 1)
            { }
            else
            {
                byte targetField;
                targetField = chessman.HistoryLocations.LastOrDefault(i => i.Key < _move).Value;
                chessman.Location = targetField;
                chessman.HistoryLocations.Remove(_move);
                chessman.AfterMove();
                GraphicsDisp.ChangeChessmanDisplay(chessman);
                Array.Copy(_chessboard, currField, _chessboard, targetField, 1);
            }
            GraphicsDisp.RemoveChessmanDisplay(currField);
            _chessboard[currField] = null;

            Chessman capturedChessman = _capturedChessman.FirstOrDefault(i => i.HistoryLocations.ContainsKey(_move));
            if (capturedChessman != null)
            {
                capturedChessman.HistoryLocations.Remove(_move);
                if (!capturedChessman.FromPromotion)
                    capturedChessman.Location = currField;
                else
                {
                    capturedChessman.Location = (byte)(currField - capturedChessman.AllowedMove[0]);
                    capturedChessman.FromPromotion = false;
                }
                capturedChessman.Captured = false;
                GraphicsDisp.ChangeChessmanDisplay(capturedChessman);
                _chessboard[capturedChessman.Location] = capturedChessman;
                _capturedChessman.Remove(capturedChessman);
                capturedChessman.AfterMove();
            }
            CheckIsCheck((GameEvents.CurrentTeam == Teams.Black) ? Teams.White : Teams.Black);
            return true;
        }

        /// <summary>
        /// Promocja pionka w ybraną figurę
        /// </summary>
        /// <param name="chessman"></param>
        private void PawnPromotionCheck(Chessman chessman)
        {
            if (chessman.GetType() != typeof(Pawn))
                return;
            if (chessman.Location % 8 > 0 && chessman.Location % 8 < 7)
                return;
            PawnChange f = new PawnChange();
            f.ShowDialog();
            Chessman newChessman;
            switch (_pawnTransform)
            {
                case "rook":
                    newChessman = new Rook(chessman.Team, chessman.Location);
                    break;
                case "knight":
                    newChessman = new Knight(chessman.Team, chessman.Location);
                    break;
                case "bishop":
                    newChessman = new Bishop(chessman.Team, chessman.Location);
                    break;
                case "queen":
                    newChessman = new Queen(chessman.Team, chessman.Location);
                    break;
                default:
                    return;
            }
            _pawnTransform = string.Empty;
            newChessman.FromPromotion = true;

            chessman.Captured = true;
            chessman.FromPromotion = true;
            _capturedChessman.Add(chessman);

            newChessman.AddPreviousLocation(_move, newChessman.Location);
            _chessboard[chessman.Location] = newChessman;
            GraphicsDisp.ChangeChessmanDisplay(newChessman);
        }

        /// <summary>
        /// Funkcja wywoływana po poprawnym ruchu figury - cofa ruch jeżeli król jest nadal szachowany
        /// </summary>
        public void AfterMove()
        {
            if (!_checkForCheckAfterMove)
                return;
            if (CheckIsCheck((GameEvents.CurrentTeam == Teams.Black) ? Teams.White : Teams.Black))
                GameEvents.Undo();
            _checkForCheckAfterMove = false;
        }

        /// <summary>
        /// Funkcje przesuwająca figure gracza komputerowego
        /// </summary>
        /// <param name="chessman"></param>
        /// <param name="location"></param>
        /// <param name="callerFile"></param>
        public void MoveCPUChessman(Chessman chessman, byte location,
            [CallerFilePath]string callerFile = "")
        {
            if (!callerFile.EndsWith("CPUPlayer.cs"))
                return;                
            MoveChessmanIntoField(chessman, location, true);
        }

        /// <summary>
        /// Funkcja sprawdzająca wynik ruchu figury gracza komputerowero
        /// </summary>
        /// <param name="chessman"></param>
        /// <param name="location"></param>
        /// <param name="callerFile"></param>
        /// <returns></returns>
        public MovesFinish? GetCPUMoveFinish(Chessman chessman, byte location,
            [CallerFilePath]string callerFile = "")
        {
            if (!callerFile.EndsWith("CPUPlayer.cs"))
                return null;
            return IsMovePossible(chessman, location);
        }

        /// <summary>
        /// Czy figura gracza komputerowego może być zbita
        /// </summary>
        /// <param name="chessman"></param>
        /// <param name="callerFile"></param>
        /// <returns></returns>
        public bool? CanCPUFigureBeCapture(Chessman chessman,
            [CallerFilePath]string callerFile = "")
        {
            if (!callerFile.EndsWith("CPUPlayer.cs"))
                return null;
            return CanChekingChessmanCapture(chessman);
        }

    }
}
