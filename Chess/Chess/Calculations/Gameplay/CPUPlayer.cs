﻿using Chess.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Calculations.Gameplay
{
    public static class CPUPlayer
    {
        private static Chessboard _chessboard;
        private static byte _openVariant;

        public static void StartCPUMove(Chessboard chessboard)
        {
            _chessboard = chessboard;

            if (_chessboard.Move < 5)
                CPUOpen(_chessboard.Move);
            else
                MoveCalculate();
        }

        private static void CPUOpen(int move)
        {
            switch (move)
            {
                case 2:
                    if (_chessboard.ChessboardView[36] == null)
                    {
                        _openVariant = 1;
                        _chessboard.MoveCPUChessman(_chessboard.ChessboardView[25], 27);
                    }
                    else
                    {
                        _openVariant = 2;
                        _chessboard.MoveCPUChessman(_chessboard.ChessboardView[33], 35);
                    }
                    break;
                case 4:
                    if (_openVariant == 1)
                        _chessboard.MoveCPUChessman(_chessboard.ChessboardView[33], 34);
                    else
                        _chessboard.MoveCPUChessman(_chessboard.ChessboardView[25], 26);
                    break;
            }
        }

        private static void MoveCalculate()
        {
            MovesList movesList = new MovesList();

            foreach(Chessman chessman in _chessboard.ChessboardView
                .Where(i => i != null)
                .Where(i => i.Team == Teams.Black))
            {
                foreach (byte m in chessman.AllowedMove)
                {
                    byte newLocation = (byte)(chessman.Location + m);
                    MovesFinish? result = _chessboard.GetCPUMoveFinish(chessman, newLocation);

                    while (result != null)
                    {
                        if (result != MovesFinish.Protection)
                            movesList.Add(new MoveMemory(chessman, newLocation, result));
                        if (result != MovesFinish.Move)
                            break;
                        newLocation = (byte)(newLocation + m);
                        result = _chessboard.GetCPUMoveFinish(chessman, newLocation);
                    }
                }
            }
            int index = ChooseBestMove(movesList);
            MoveMemory bestMove = movesList.Item(index);
            _chessboard.MoveCPUChessman(bestMove.GetChessman, bestMove.Location);
        }

        private static int ChooseBestMove(MovesList moveList)
        {
            Dictionary<int, Chessman> captureList = new Dictionary<int, Chessman>();
            Dictionary<int, Chessman> checkList = new Dictionary<int, Chessman>();
            Dictionary<int, Chessman> movesList = new Dictionary<int, Chessman>();

            foreach (MoveMemory moveMemory in moveList)
            {
                switch (moveMemory.Move)
                {
                    case MovesFinish.Move:
                        movesList.Add(moveMemory.Id,
                        _chessboard.ChessboardView[moveMemory.Location]);
                        break;
                    case MovesFinish.Capture:
                        captureList.Add(moveMemory.Id,
                        _chessboard.ChessboardView[moveMemory.Location]);
                        break;
                    case MovesFinish.Check:
                        checkList.Add(moveMemory.Id,
                        _chessboard.ChessboardView[moveMemory.Location]);
                        break;
                    case MovesFinish.EnPassant:
                        captureList.Add(moveMemory.Id,
                        _chessboard.ChessboardView[moveMemory.Location]);
                        break;
                }
            }

            if(checkList.Count > 0)
            {
                return checkList.First().Key;
            }
            else if(captureList.Count > 0)
            {
                return GetHighestChessman(captureList);
            }
            else
            {
                Random rnd = new Random();
                int month = rnd.Next(1, movesList.Count + 1);
                return movesList.Skip(month).First().Key;
            }
        }

        private static int GetHighestChessman(Dictionary<int, Chessman> dict)
        {
            for (int i = 5; i >= 0; i--)
            {
                string chessman = ((ChessmansValues)i).ToString().ToLower();
                foreach (KeyValuePair<int, Chessman> p in dict)
                {
                    if (p.Value.GetType().ToString().ToLower().Contains(chessman))
                        return p.Key;
                }
            }
            return -1;
        }

        private static bool Val1GreaterThanVal2 (Chessman val1, Chessman val2)
        {
            int res1 = -1;
            int res2 = -1;
            foreach (ChessmansValues i in Enum.GetValues(typeof(ChessmansValues)))
            {
                if (val1.GetType().ToString().ToLower()
                    .Contains(i.ToString().ToLower()))
                    res1 = (int)i;
                if (val2.GetType().ToString().ToLower()
                    .Contains(i.ToString().ToLower()))
                    res2 = (int)i;
            }
            if (res1 >= res2)
                return true;
            else
            return false;
        }
    }
}
