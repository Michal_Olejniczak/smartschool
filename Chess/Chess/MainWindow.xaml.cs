﻿using System;
using System.Windows;
using System.Windows.Controls;
using Chess.Calculations.Gameplay;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using Chess.Forms;

namespace Chess
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private const int GWL_STYLE = -16;
        private const int WS_MAXIMIZEBOX = 0x10000;
        private bool _btnSelected = false;
        internal static MainWindow main;

        public MainWindow()
        {
            InitializeComponent();
            main = this;
            GraphicsDisp.PrepeareChessboard();
            GraphicsDisp.SetWhiteTimer(new TimeSpan(0, 0, 0));
            GraphicsDisp.SetBlackTimer(new TimeSpan(0, 0, 0));
            SourceInitialized += new EventHandler(Window_SourceInitialized);
        }

        private void btnClick(object sender, RoutedEventArgs e)
        {
            if (!GameEvents.GameStarted)
                return;

            Button btn = (Button)sender;

            if (_btnSelected)
            {
                GameEvents.PerformMove(btn);
            }
            else
            {
                GameEvents.ShowPossibleMove(btn);
            }
            _btnSelected = !_btnSelected;
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.Height = this.Width - 175;
            gridMain.ColumnDefinitions[1].Width = new GridLength(236);
        }

        private void Window_SourceInitialized(object sender, EventArgs e)
        {
            var hwnd = new WindowInteropHelper((Window)sender).Handle;
            var value = GetWindowLong(hwnd, GWL_STYLE);
            SetWindowLong(hwnd, GWL_STYLE, (int)(value & ~WS_MAXIMIZEBOX));
        }

        private void btn_NewGameClick(object sender, RoutedEventArgs e)
        {
            if (!GameEvents.GameStarted)
                StartGame(false);
            else
                StartGame(true);
        }

        private void StartGame(bool reset)
        {
            if (reset)
            {
                if (MessageBox.Show("Are you realy want to restart game?", "Restart game",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    GameEvents.GameEnd(true);
                else
                    return;
            }
            btn_NewGame.Content = "Restart game";
            GameMode f = new GameMode();
            if (f.ShowDialog() == true)
                GameEvents.StartGame(false);
            else
                GameEvents.StartGame(true);
        }

        private void btn_CloseClick(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Close program?", "Exit",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                Close();
        }

        internal string CurrentPlayerLblContent
        {
            get { return lbl_CurrentPlayer.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { lbl_CurrentPlayer.Content = value; })); }
        }

        internal string WhiteTimerLblContent
        {
            get { return lbl_PlrWhiteTimer.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { lbl_PlrWhiteTimer.Content = value; })); }
        }

        internal string BlackTimerLblContent
        {
            get { return lbl_PlrBlackTimer.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { lbl_PlrBlackTimer.Content = value; })); }
        }

        internal string CheckLblContent
        {
            get { return lbl_CheckLabel.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { lbl_CheckLabel.Content = value; })); }
        }

        internal string ThrowGameEnd
        {
            set { Dispatcher.Invoke(new Action(() => { GameEnd(value); })); }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            GameEvents.Dispose();
        }

        private void GameEnd(string team)
        {
            if (team.Length > 0)
            {
                if (MessageBox.Show("Congratulation! Player " + team + " won! \n Start new game?", "Game end",
                        MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    StartGame(true);
            }
            else
            {
                if (MessageBox.Show("Its a draw! \n Start new game?", "Game end",
                        MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    StartGame(true);
            }
        }

        private void btn_Undo_Click(object sender, RoutedEventArgs e)
        {
            if (btn_NewGame.Content.ToString() == "Restart game")
                GameEvents.Undo();
        }
    }
}
